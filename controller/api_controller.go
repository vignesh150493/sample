package controller

import (
	"log"
	"time"
)

type MiddleLayer struct {
	Name string
	Receiver chan string
}

func ApiCall(middleMan MiddleLayer) {
	log.Println("Request by Middleman ", middleMan.Name)
	time.Sleep(5 * time.Second)
	middleMan.Receiver <- "Data is put!!"
}