package interactor

import "sample/controller"

type MobileClient struct {
	Name string
	Receiver chan string
}


func GetData(someClient MobileClient) {
	middleMan := controller.MiddleLayer{Name: someClient.Name + " middleman", Receiver: make(chan string)}

	go controller.ApiCall(middleMan)

	select {
	case dataFromController := <-middleMan.Receiver:
		someClient.Receiver <- dataFromController
	}
	//log.Println("Request by ", someClient.Name)
	//time.Sleep(5 * time.Second)
	//someClient.Receiver <- "Data is put!!"
}
