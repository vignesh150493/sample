package main

import (
	"log"
	"sample/interactor"
)

var Logger *log.Logger



func main() {

	client1 := interactor.MobileClient{Name: "Client1", Receiver: make(chan string)}
	client2 := interactor.MobileClient{Name: "Client2", Receiver: make(chan string)}

	go interactor.GetData(client1)

	go interactor.GetData(client2)

	log.Println("Received by MobileClient ", client1.Name, <-client1.Receiver)
	log.Println("Received by MobileClient ", client2.Name, <-client2.Receiver)
}


//func apiCall(someClient interactor.MobileClient) {
//	log.Println("Request by ", someClient.Name)
//	time.Sleep(5 * time.Second)
//	someClient.Receiver <- "Data is put!!"
//}
